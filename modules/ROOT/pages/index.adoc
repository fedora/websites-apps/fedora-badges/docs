include::{partialsdir}/attributes.adoc[]

= Fedora Badges

{BADGES-HOME}[Fedora Badges] is a fun website built to recognize contributors to the Fedora Project, help new and existing Fedora contributors find different ways to get involved, and encourage the improvement of Fedora's infrastructure.

== How does Badges work?

It's really easy!
Sign into Badges with your Fedora account, and you'll see you have at least one badge right away.
Congratulations - you're a Badger!
If you participate in Fedora in any way, you'll probably notice Badges popping up on your profile as you go about your business, though sadly we don't cover every area of Fedora yet - we're doing our best to make sure we reward as many forms of participation as we can!

Want to see how your badge collection compares with others?
Check the Leaderboard.
Jonesing for more badges?
You can check the Badge index to see all the badges and get to work on your collection!
Click on a badge to see how to get it - but we intentionally didn't spell it all out exactly.
Part of the fun is figuring it out!

Another cool thing: the Badges site is mobile-optimized, so you can easily keep track of your badges on the go!
