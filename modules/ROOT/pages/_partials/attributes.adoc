:BADGES-HOME: https://badges.fedoraproject.org/
:BADGES-USER: https://badges.fedoraproject.org/user
:COMMBLOG: https://communityblog.fedoraproject.org
:FEDMSG: http://www.fedmsg.com/
:FWIKI: https://fedoraproject.org/wiki
:YEAR: 2020
